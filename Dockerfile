FROM python:3.9

ADD main.py .

ADD poetry.lock .

ADD pyproject.toml .

RUN pip install --upgrade pip

RUN pip install pandas

RUN pip install torch==1.12.1+cpu torchvision==0.13.1+cpu torchaudio==0.12.1 --extra-index-url https://download.pytorch.org/whl/cpu

CMD python main.py
